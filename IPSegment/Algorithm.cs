﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace IPSegment
{
    class Algorithm
    {
        public static string findSegment<T>(T start, T end, Action<T> inc)
            where T : IComparable, ICloneable
        {
            T temp = (T)start.Clone();
            string output = "";

            do
            {
                output += temp.ToString() + "\t";
                inc(temp);
            } while (temp.CompareTo(end) <= 0);

            return output;
        }
    }
}
