﻿namespace IPSegment
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxRight = new System.Windows.Forms.TextBox();
            this.textBoxLeft = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.SearchSegmentButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxRight
            // 
            this.textBoxRight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxRight.Location = new System.Drawing.Point(285, 12);
            this.textBoxRight.Name = "textBoxRight";
            this.textBoxRight.Size = new System.Drawing.Size(267, 20);
            this.textBoxRight.TabIndex = 0;
            this.textBoxRight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxRight_KeyPress);
            // 
            // textBoxLeft
            // 
            this.textBoxLeft.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxLeft.Location = new System.Drawing.Point(13, 12);
            this.textBoxLeft.Name = "textBoxLeft";
            this.textBoxLeft.Size = new System.Drawing.Size(266, 20);
            this.textBoxLeft.TabIndex = 1;
            this.textBoxLeft.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxLeft_KeyPress);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(13, 68);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox3.Size = new System.Drawing.Size(539, 302);
            this.textBox3.TabIndex = 2;
            // 
            // SearchSegmentButton
            // 
            this.SearchSegmentButton.Location = new System.Drawing.Point(285, 39);
            this.SearchSegmentButton.Name = "SearchSegmentButton";
            this.SearchSegmentButton.Size = new System.Drawing.Size(266, 23);
            this.SearchSegmentButton.TabIndex = 3;
            this.SearchSegmentButton.Text = "Поиск";
            this.SearchSegmentButton.UseVisualStyleBackColor = true;
            this.SearchSegmentButton.Click += new System.EventHandler(this.SearchSegmentButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 382);
            this.Controls.Add(this.SearchSegmentButton);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBoxLeft);
            this.Controls.Add(this.textBoxRight);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxRight;
        private System.Windows.Forms.TextBox textBoxLeft;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button SearchSegmentButton;
    }
}

