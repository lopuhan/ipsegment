﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPSegment
{
    public class IpAddress : IComparable, ICloneable
    {
        byte[] address;
        int length;

        public int Length
        {
            get { return length; }
        }

        public IpAddress(string input)
        {
            string[] numbers = input.Split('.');

            if (numbers.Length != 4 && numbers.Length != 6)
                throw new ArgumentException("Не является IPv6 или IPv4 адресом.");

            length = numbers.Length;
            address = new byte[length];

            for (int i = 0; i < length; i++)
            {
                try
                {
                    address[i] = byte.Parse(numbers[i]);
                }
                catch
                {
                    throw new ArgumentException(string.Format("Неверное число в {0} слоте.", i + 1));
                }
            }
        }

        public int CompareTo(object obj)
        {
            for (int i = 0; i < length; i++)
            {
                if (address[i] < ((IpAddress)obj).address[i]) return -1;

                if (address[i] > ((IpAddress)obj).address[i]) return 1;
            }

            return 0;
        }

        public void Increment()
        {
            for (int i = length - 1; i >= 0; i--)
            {
                address[i]++;
                if (address[i] != 0)
                    return;
            }

            throw new OverflowException();
        }

        public static IpAddress operator ++(IpAddress x)
        {
            int rightNumberIndex = x.length - 1;

            for (int i = rightNumberIndex; i >= 0; i--)
            {
                x.address[i]++;
                if (x.address[i] != 0)
                    return x;
            }

            throw new OverflowException();
        }

        public override string ToString()
        {
            string res = "";
            
            foreach (byte b in address)
                res += b.ToString() + '.';


            return res.Substring(0, res.Length - 1);
        }

        public object Clone()
        {
            IpAddress res = new IpAddress(this.ToString());

            return res;
        }
    }
}
