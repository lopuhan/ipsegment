﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IPSegment
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void SearchSegmentButton_Click(object sender, EventArgs e)
        {
            IpAddress first, second;

            try
            {
                first = new IpAddress(textBoxLeft.Text);
                second = new IpAddress(textBoxRight.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

            if (first.CompareTo(second) > 0)
            {
                MessageBox.Show("Левый адрес больше правого.");
                return;
            }

            if (first.Length != second.Length)
            {
                MessageBox.Show("Адреса разных длин.");
                return;
            }

            textBox3.Clear();

            textBox3.Text += Algorithm.findSegment<IpAddress>(first, second, (x) => x.Increment());          
        }

        private void textBoxLeft_KeyPress(object sender, KeyPressEventArgs e)
        {
            HandleKeyPress(sender, e);
        }

        private void textBoxRight_KeyPress(object sender, KeyPressEventArgs e)
        {
            HandleKeyPress(sender, e);
        }

        private void HandleKeyPress(object sender, KeyPressEventArgs e)
        {            
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }
    }
}
